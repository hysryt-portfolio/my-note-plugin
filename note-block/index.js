( function ( wp ) {
  var el = wp.element.createElement;
  var name = 'wp-block-my-note-plugin-note-block';

  function createEditElement( props ) {
    return el(
      'div', {
        className: name
      }, [
        el(
          wp.editor.RichText, {
            tagName: 'p',
            className: name + '__heading',
            value: props.attributes.heading,
            onChange: function( heading ) {
              props.setAttributes( { heading: heading } );
            }
          }
        ),
        el(
          wp.editor.RichText, {
            tagName: 'div',
            className: name + '__content',
            value: props.attributes.content,
            multiline: 'p',
            onChange: function( content ) {
              props.setAttributes( { content: content } );
            }
          }
        )
      ]
    );
  }

  function createSaveElement( props ) {
    return el(
      'div', {
      }, [
        el(
          wp.editor.RichText.Content, {
            tagName: 'p',
            className: name + '__heading',
            value: props.attributes.heading
          }
        ),
        el(
          wp.editor.RichText.Content, {
            tagName: 'div',
            className: name + '__content',
            value: props.attributes.content
          }
        )
      ]
    );
  }

  wp.blocks.registerBlockType( 'my-note-plugin/note-block', {
    title: '備考ブロック',
    category: 'common',

    attributes: {
      heading: {
        type: 'string',
        source: 'children',
        selector: 'p.' + name + '__heading',
        default: 'Note'
      },
      content: {
        type: 'array',
        source: 'children',
        selector: 'p.' + name + '__content'
      }
    },
    
    edit: createEditElement,
    save: createSaveElement
  } );
} )( window.wp );