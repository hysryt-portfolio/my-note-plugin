<?php
/**
 * Plugin Name:     My Note Plugin
 * Version:         0.1
 */

add_action( 'init', function() {
  if ( ! function_exists( 'register_block_type' ) ) {
    return;
  }
  $dir = dirname( __FILE__ );
  $index_js = 'note-block/index.js';

  wp_register_script(
    'note-block-block-editor',
    plugins_url( $index_js, __FILE__ ),
    array(
      'wp-blocks',
      'wp-element',
    ),
    filemtime( "$dir/$index_js" )
  );

  $editor_css = 'note-block/editor.css';
  wp_register_style(
    'note-block-block-editor',
    plugins_url( $editor_css, __FILE__ ),
    array(),
    filemtime( "$dir/$editor_css" )
  );

  $style_css = 'note-block/style.css';
  wp_register_style(
    'note-block-block',
    plugins_url( $style_css, __FILE__ ),
    array(),
    filemtime( "$dir/$style_css" )
  );

  register_block_type( 'my-note-plugin/note-block', array(
    'editor_script' => 'note-block-block-editor',
    'editor_style'  => 'note-block-block-editor',
    'style'         => 'note-block-block',
  ) );
} );